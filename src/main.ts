import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';
import { LoggingInterceptor } from './Interceptor/logging.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  app.useGlobalInterceptors(new LoggingInterceptor());
  await app.useGlobalPipes(new ValidationPipe());
  const config = app.get<ConfigService>(ConfigService);
  await app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.NATS,
    options: config.get('nats.options'),
  });
  await app.startAllMicroservices();
  await app.listen(config.get('port') || 5000, () => {
    console.log(new Date(), `start listen on port ${config.get('port')}`);
  });
}
bootstrap();
