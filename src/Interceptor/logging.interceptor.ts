import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger,
  BadRequestException,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  private readonly logger = new Logger(LoggingInterceptor.name);
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const url = context['args'][0].url;
    const method = context['args'][0].method;
    this.logger.log('REQUEST: {' + url + '  ,' + method + '} ');

    return next.handle().pipe(
      catchError((err) => {
        if (err.name === 'BadRequestException') {
          this.logger.log(
            'ERROR:  {' +
              url +
              '  ,' +
              method +
              '} ' +
              'MESSAGE:  ' +
              err.response.PropertiesException.Message,
          );
          return throwError(() => new BadRequestException(err));
        } else {
          this.logger.log(
            'ERROR:  {' + url + '  ,' + method + '} ' + 'ERROR:  ' + err,
          );
          return throwError(() => err);
        }
      }),
      tap((e) =>
        this.logger.log(
          'RESPONESE:    {' + url + '  ,' + method + '}  SUCCESS',
        ),
      ),
    );
  }
}
