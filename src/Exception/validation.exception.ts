import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  UnauthorizedException,
  HttpStatus,
  Logger,
  BadRequestException,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { CONFIG_MESSTYPE } from 'src/common/ConfigMesstype';
import { FormSendException } from 'src/Type/Type';
import { ERROR_CODE } from '../common/ErrorConfig';

@Catch(UnauthorizedException)
export class AuthorExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(AuthorExceptionFilter.name);
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: any, host: ArgumentsHost): void {
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();
    const reqBody = ctx['args'][0].body;
    reqBody.Header.Request_ID = reqBody.Header.Message_Type;
    const url = ctx['args'][0].url;
    const method = ctx['args'][0].method;
    console.log(url, method);
    const messType =
      CONFIG_MESSTYPE.find((e) => e.req === reqBody.Header.Message_Type)?.res ||
      '';
    reqBody.Header.Message_Type = messType;
    const error = ERROR_CODE.ERROR_UN_AUTHOR;
    const reply: FormSendException = {
      Header: reqBody.Header,
      Data: {},
      Error: error,
      Signature: reqBody.Signature,
      PropertiesException: {
        Name: exception.name,
        Message: exception.response.message,
      },
    };
    this.logger.log('REQUEST:   {' + url + ', ' + method + '}   UNAUTHOR');
    const httpStatus = HttpStatus.OK;
    httpAdapter.reply(ctx.getResponse(), reply, httpStatus);
  }
}

@Catch(BadRequestException)
export class ExceptionMicroserviceFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: any, host: ArgumentsHost): void {
    const { httpAdapter } = this.httpAdapterHost;
    const ctx = host.switchToHttp();
    const resBody = exception.response.response;
    const httpStatus = HttpStatus.OK;
    httpAdapter.reply(ctx.getResponse(), resBody, httpStatus);
  }
}
@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(AllExceptionFilter.name);
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}
  catch(exception: any, host: ArgumentsHost): void {
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();
    const reqBody = ctx['args'][0].body;
    reqBody.Header.Request_ID = reqBody.Header.Message_Type;

    const messType =
      CONFIG_MESSTYPE.find((e) => e.req === reqBody.Header.Message_Type)?.res ||
      '';
    reqBody.Header.Message_Type = messType;
    const error = ERROR_CODE.ERROR_SERVER;
    const reply: FormSendException = {
      Header: reqBody.Header,
      Data: {},
      Error: error,
      Signature: reqBody.Signature,
      PropertiesException: {
        Exception: exception.toString(),
      },
    };
    const httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    httpAdapter.reply(ctx.getResponse(), reply, httpStatus);
  }
}
