import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { PspModule } from './psp/psp.module';

import { HaiquanModule } from './haiquan/haiquan.module';
import configuration from './config/configuration';
import { En_DecodeData } from './common/En_DecodeData';


@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    PspModule,
    HaiquanModule,
  ],
  providers: [
    {
      provide: 'GATEWAY_NATS',
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return ClientProxyFactory.create({
          transport: Transport.NATS,
          options: configService.get('nats.options'),
        });
      },
    },
  ],
  exports: ['GATEWAY_NATS'],
})
export class AppModule {}
