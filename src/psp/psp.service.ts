import { Injectable } from '@nestjs/common';
import { En_DecodeData } from 'src/common/En_DecodeData';
import { SendReq } from 'src/common/SendReq';

@Injectable()
export class PspService {
  constructor(
    private readonly sendReq: SendReq,
    private readonly en_decode: En_DecodeData,
  ) {}

  async ReqToken(reqTokenDto: any) {
    const to: string = 'request-Token-Haiquan';
    return this.sendReq.SendReq(reqTokenDto, to);
  }
  async ReqInitRequestToPay(reqInitRequestToPayDto: any) {
    const to: string = 'request-InitRequestToPay';
    return this.sendReq.SendReq(reqInitRequestToPayDto, to);
  }
  async ReqEncryptData(reqEncryptDataDto: any) {
    const json = JSON.stringify(reqEncryptDataDto.Data);
    const data_encrypt = this.en_decode.encrypt(json);
    reqEncryptDataDto.Data = { data_encrypt };
    return reqEncryptDataDto;
  }
}
