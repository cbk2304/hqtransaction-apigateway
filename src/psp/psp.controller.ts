import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { IsJSON } from 'class-validator';
import { En_DecodeData } from 'src/common/En_DecodeData';
import { AuthenticationGuard } from 'src/guards/auth.guards';

import { PspService } from './psp.service';

@Controller('ipgateway')
export class PspController {
  constructor(private readonly pspService: PspService) {}

  @Post('Token')
  async reqToken(@Body() reqPspTokenDto: any) {
    return await this.pspService.ReqToken(reqPspTokenDto);
  }
  @UseGuards(AuthenticationGuard)
  @Post('InitRequestToPay')
  async reqInitRequestToPay(@Body() reqPspInitRequestToPayDto: any) {
    return await this.pspService.ReqInitRequestToPay(reqPspInitRequestToPayDto);
  }
  @Post('Encrypt')
  async reqEncrypt(@Body() reqPspEncryptDto: any) {
    return await this.pspService.ReqEncryptData(reqPspEncryptDto);
  }
}
