import { Module } from '@nestjs/common';
import { PspService } from './psp.service';
import { PspController } from './psp.controller';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_FILTER } from '@nestjs/core';
import { SendReq } from 'src/common/SendReq';
import { JsonWebTokenStrategy } from 'src/statregies/jwt.strategy';
import {
  AuthorExceptionFilter,
  ExceptionMicroserviceFilter,
  AllExceptionFilter,
} from 'src/Exception/validation.exception';
import { En_DecodeData } from 'src/common/En_DecodeData';

@Module({
  imports: [
    ConfigModule.forRoot(),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) =>
        configService.get('jwt'),
      inject: [ConfigService],
    }),
  ],
  controllers: [PspController],
  providers: [
    PspService,
    SendReq,
    En_DecodeData,
    JsonWebTokenStrategy,
    {
      provide: APP_FILTER,
      useClass: AuthorExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: AllExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: ExceptionMicroserviceFilter,
    },
  ],
})
export class PspModule {}
