export class FormSendException {
  Header: object;
  Data: object;
  Error: object;
  Signature: object;
  PropertiesException: object;
}
