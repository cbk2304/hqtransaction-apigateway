import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createCipheriv, createDecipheriv } from 'crypto';

@Injectable()
export class En_DecodeData {
  constructor(private configService: ConfigService) {}
  private key = this.configService.get('en_decodeData.key');

  encrypt(text) {
    const iv = this.configService.get('en_decodeData.iv');
    const cipher = createCipheriv('aes-256-cbc', Buffer.from(this.key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return encrypted.toString('hex');
  }

  decrypt(text) {
    let iv = this.configService.get('en_decodeData.iv');
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    let decipher = createDecipheriv('aes-256-cbc', Buffer.from(this.key), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
  }
}
