import { Inject } from '@nestjs/common';
import { ClientNats } from '@nestjs/microservices';
import { En_DecodeData } from './En_DecodeData';

export class SendReq {
  constructor(
    private readonly en_decode: En_DecodeData,
    @Inject('GATEWAY_NATS') private readonly natsClient: ClientNats,
  ) {}
  async SendReq(reqtoken: any, to: string) {
    const data = reqtoken.Data;
    const decodedata = this.en_decode.decrypt(data);
    reqtoken.Data = JSON.parse(decodedata);
    return await this.natsClient.send(to, reqtoken);
  }
}
