import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthenticationGuard } from 'src/guards/auth.guards';



import { HaiquanService } from './haiquan.service';

@Controller('customs')
export class HaiquanController {
  constructor(private readonly pspService: HaiquanService) {}

  @Post('Token')
  async reqToken(@Body() reqhaiquanTokenDto: any) {
    return await this.pspService.ReqToken(reqhaiquanTokenDto);
  }
  @UseGuards(AuthenticationGuard)
  @Post('ProcessingRequest')
  async processingRequest(@Body() reqhaiquanPRDto: any) {
    return await this.pspService.ReqProcessingRequest(reqhaiquanPRDto);
  }
  @UseGuards(AuthenticationGuard)
  @Post('GetProcessingStatus')
  async getProcessingStatus(@Body() reGPSDto: any) {
    return await this.pspService.ReqGPSD(reGPSDto);
  }
  @UseGuards(AuthenticationGuard)
  @Post('GetUidDetailInfo')
  async getUidDetailInfo(@Body() reqGUDIDto: any) {
    return await this.pspService.ReqGUDI(reqGUDIDto);
  }
  @UseGuards(AuthenticationGuard)
  @Post('UpdateBankProcessStatus')
  async updateBankProcessStatus(
    @Body() reqUBPSDto: any,
  ) {
    return await this.pspService.ReqUBPS(reqUBPSDto);
  }
}
