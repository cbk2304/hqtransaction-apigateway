import { Module } from '@nestjs/common';
import { HaiquanService } from './haiquan.service';
import { HaiquanController } from './haiquan.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { APP_FILTER } from '@nestjs/core';
import { JsonWebTokenStrategy } from 'src/statregies/jwt.strategy';
import { SendReq } from 'src/common/SendReq';
import { AuthorExceptionFilter } from 'src/Exception/validation.exception';
import { En_DecodeData } from 'src/common/En_DecodeData';


@Module({
  imports: [
    ConfigModule.forRoot(),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) =>
        configService.get('jwt'),
      inject: [ConfigService],
    }),
  ],
  controllers: [HaiquanController],
  providers: [
    HaiquanService,
    SendReq,
    En_DecodeData,
    JsonWebTokenStrategy,
    {
      provide: APP_FILTER,
      useClass: AuthorExceptionFilter,
    },
  ],
})
export class HaiquanModule {}
