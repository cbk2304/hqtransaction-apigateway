import { Injectable } from '@nestjs/common';
import { SendReq } from 'src/common/SendReq';



@Injectable()
export class HaiquanService {
  constructor(private readonly sendReq: SendReq) {}

  async ReqToken(reqTokenDto: any) {
    const to: string = 'request-Token-Psp';
    return await this.sendReq.SendReq(reqTokenDto, to);
  }
  async ReqProcessingRequest(reqRPDto: any) {
    const to: string = 'request-ProcessingRequest';
    return await this.sendReq.SendReq(reqRPDto, to);
  }
  async ReqUBPS(reqUBPSDto: any) {
    const to: string = 'request-UpdateBankProcessStatus';
    return this.sendReq.SendReq(reqUBPSDto, to);
  }
  async ReqGUDI(reqGUDIDto: any) {
    const to: string = 'request-GetUidDetailInfo';
    return await this.sendReq.SendReq(reqGUDIDto, to);
  }
  async ReqGPSD(reGPSDto: any) {
    const to: string = 'request-GetProcessingStatus';
    return await this.sendReq.SendReq(reGPSDto, to);
  }
}
